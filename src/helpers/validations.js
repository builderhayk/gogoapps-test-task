export const subdomainRegexTest = (subdomain) =>
    new RegExp(/^(?!-)[A-Za-z0-9-]{3,63}(?<!-)$/).test(subdomain);

export const phoneRegexTest = (phone) =>
    new RegExp(/^(1\s?)?((\([0-9]{3}\))|[0-9]{3})[\s\-]?[\0-9]{3}[\s\-]?[0-9]{4}$/).test(phone);

export const sanitizePhoneNumber = (str) => str.replace(/[^0-9]/g, "");
