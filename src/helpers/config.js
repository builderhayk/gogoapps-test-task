require("dotenv").config();

export const apiUrl = process.env.API_URL;
export const appUrl = process.env.APP_URL;
export const apiPort = process.env.PORT;

export const corsOrigins = process.env.CORS_ORIGINS
        ?.split(",");

export const picturesApiKey = process.env.NODE_ENV === "production" ? process.env.API_KEY : process.env.DEMO_KEY;
export const picturesApiUrl = process.env.PICTURES_API_URL;
export const concurrentRequests = process.env.CONCURRENT_REQUESTS;

