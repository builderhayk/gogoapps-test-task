import { DATE_FORMAT_STRING } from "../appConfig/constants";
import moment from "moment";

export default class Utils {
    static divideDateRangeIntoChunks(start_date, end_date, days_to_divide) {
        const result = [];
        const current = new Date(start_date.getTime());

        do {
            const d1 = new Date(current.getTime());
            const d2 = new Date(current.setDate(current.getDate() + days_to_divide));

            result.push({
                start_date: moment(d1).format(DATE_FORMAT_STRING),
                end_date: d2 <= end_date ? moment(d2.setDate(d2.getDate() - 1)).format(DATE_FORMAT_STRING) : moment(end_date).format(DATE_FORMAT_STRING)
            });
        } while (current <= end_date);

        return result;
    };
}
