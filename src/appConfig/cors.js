import params from "./params";

const corsOptions = {
    development: {
        origin: "*",
        credentials: true,
        allowedHeaders: [
            "Content-Type",
            "Authorization"
        ]
    },
    production: {
        origin: params?.production?.corsOrigins || "*",
        credentials: true,
        allowedHeaders: [
            "Content-Type",
            "Authorization"
        ]
    }
};

export default corsOptions[process.env.NODE_ENV || "development"];
