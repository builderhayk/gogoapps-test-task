import {
    apiUrl,
    appUrl,
    apiPort,
    corsOrigins,
    picturesApiKey,
    picturesApiUrl,
    concurrentRequests
} from "../helpers/config";

const params = {
    development: {
        apiUrl,
        appUrl,
        apiPort,
        corsOrigins,
        picturesApiKey,
        picturesApiUrl,
        concurrentRequests
    },
    production: {
        apiUrl,
        appUrl,
        apiPort,
        corsOrigins,
        picturesApiKey,
        picturesApiUrl,
        concurrentRequests
    }
};

export default params[process.env.NODE_ENV || "development"];
