import moment from "moment";
import { concurrentRequests } from "../helpers/config";
import { TWO_MINUTES } from "../appConfig/constants";

export default class RequestLimitManagerService {
    constructor() {
        this.requestsCache = new Map();
    }

    isAbleToCall(ipAddress) {
        const userReqInfo = this.requestsCache.get(ipAddress);
        if (userReqInfo) {
            if (userReqInfo.reqCount === parseInt(concurrentRequests || 5, 10)) {
                const now = moment();
                const timeDifferance = moment(now).diff(
                    moment(userReqInfo.lastRequestTime),
                    "seconds",
                );
                if (Number(timeDifferance) < TWO_MINUTES) {
                    return false;
                } else {
                    this.requestsCache.set(ipAddress, {
                        reqCount: 1,
                        lastRequestTime: Date.now()
                    });
                }
            } else {
                this.requestsCache.set(ipAddress, {
                    reqCount: ++userReqInfo.reqCount,
                    lastRequestTime: Date.now()
                });
            }
        } else {
            this.requestsCache.set(ipAddress, {
                reqCount: 1,
                lastRequestTime: Date.now()
            });
        }
        return true;
    }
}
