import { SUCCESS_CODE } from "../../appConfig/status-codes";
import UrlCollector from "../../services/urlCollector.service";
import moment from "moment";
import { Forbidden } from "../../appConfig/errors";
import Utils from "../../helpers/utils";
import { concurrentRequests } from "../../helpers/config";

class PicturesController {
    async getPicturesUrls({ query }, res, next) {
        try {
            const { start_date, end_date } = query;
            let initialBreakpoint = 30;

            let urls;
            const timeDifferance = moment(end_date).diff(
                moment(start_date),
                "days",
            );
            if (timeDifferance > 366) {
                throw new Forbidden("Please specify lower range of date, not greater than one year.");
            }

            if (timeDifferance > initialBreakpoint) {
                if (timeDifferance / initialBreakpoint > concurrentRequests) {
                    while (timeDifferance / initialBreakpoint > concurrentRequests) {
                        initialBreakpoint += 5;
                    }
                }

                const dateChunks = Utils.divideDateRangeIntoChunks(new Date(start_date), new Date(end_date), initialBreakpoint);

                const chunksResponse = await Promise.all(dateChunks.map(({ start_date, end_date }) => {
                    return UrlCollector.getPicturesByDateRange({ start_date, end_date });
                }));


                urls = chunksResponse.filter((chunksResponse) => {
                    return chunksResponse.success;
                }).map(succeedResp => succeedResp.data).flat().map(({ url }) => url);

            } else {
                const { data } = await UrlCollector.getPicturesByDateRange({
                    start_date,
                    end_date
                });
                urls = data.map(succeedResp => succeedResp.url);
            }

            res.status(SUCCESS_CODE).send({ urls });
        } catch (e) {
            next(e);
        }
    }
}

export default PicturesController;
