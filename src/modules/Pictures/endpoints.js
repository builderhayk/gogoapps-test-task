import PicturesController from "./pictures.controller";
import schemas from "./schemas";
import middleware from "../../middlewares";

const picturesController = new PicturesController();

export default (router) => {
    router.get("/", ...middleware(schemas, "getPicturesUrls"), picturesController.getPicturesUrls);
};
