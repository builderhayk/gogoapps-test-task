import { Router } from 'express';
import PicturesEndpoints from './endpoints';

export default class AuthModule {
    apiRouter;
    router;

    constructor(apiRouter) {
        this.apiRouter = apiRouter;
        this.router = Router();
    }

    createEndpoints() {
        this.assignRouter();
        this.assignEndpoints();
    }

    assignRouter() {
        this.apiRouter.use('/pictures', this.router);
    }

    assignEndpoints() {
        PicturesEndpoints(this.router);
    }
}
