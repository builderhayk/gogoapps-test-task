import { DATE_FORMAT_STRING, INVALID, REQUIRED } from "../../appConfig/constants";
import moment from "moment";

export default {
    getPicturesUrls: {
        limitRequestsCount: true,
        validationPipe: {
            start_date: {
                in: "query",
                notEmpty: {
                    errorMessage: REQUIRED("start_date")
                },
                custom: {
                    options: (value, { req: { query } }) => {
                        const isValidDate = moment(value, DATE_FORMAT_STRING, true).isValid();
                        if (!isValidDate) {
                            throw new Error(INVALID("start_date"));
                        }

                        const todayTimestamp = new Date().getTime();
                        const startDateTimestamp = new Date(query.start_date).getTime();
                        const endDateTimeStamp = new Date(query.end_date).getTime();
                        if (todayTimestamp < startDateTimestamp) {
                            throw new Error("start_date should be not greater than today");
                        }
                        if (startDateTimestamp > endDateTimeStamp) {
                            throw new Error("start_date can't be greater than end_date");
                        }
                        return true;
                    }
                }
            },
            end_date: {
                in: "query",
                notEmpty: {
                    errorMessage: REQUIRED("end_date")
                },
                custom: {
                    options: (value, { req: { query } }) => {
                        const isValidDate = moment(value, DATE_FORMAT_STRING, true).isValid();
                        if (!isValidDate) {
                            throw new Error(INVALID("end_date"));
                        }
                        return true;
                    }
                }
            }
        }
    },

};
