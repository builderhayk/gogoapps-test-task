import App from "./app";

import { PORT } from "./utils/configVars";
const WebServer = new App();

WebServer.app.listen(PORT || 8080, () => {
    // eslint-disable-next-line no-console
    console.log(`Server is running at ${PORT}`);
});

export default WebServer;